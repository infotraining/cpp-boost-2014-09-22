#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_float.hpp>

using namespace std;

struct Modulus : public boost::static_visitor<double>
{
    template <typename T>
    typename boost::disable_if<boost::is_float<T>, double>::type operator()(const T& x) const
    {
        return abs(x);
    }

    template <typename T>
    typename boost::enable_if<boost::is_float<T>, double>::type operator()(const T& x) const
    {
        return fabs(x);
    }
};

int main()
{
    typedef boost::variant<int, float, double, complex<double> > VariantNumber;
    vector<VariantNumber> vars;
    vars.push_back(-1);
    vars.push_back(3.14F);
    vars.push_back(8.777);
    vars.push_back(-7);
    vars.push_back(complex<double>(-1, -1));

    // TODO: korzystając z mechanizmu wizytacji wypisać na ekranie moduły liczb
    Modulus mod;
    boost::transform(vars, ostream_iterator<double>(cout, " "), boost::apply_visitor(mod));

    cout << endl;
}
