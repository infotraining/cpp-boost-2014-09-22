#include <iostream>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <vector>
#include <algorithm>
#include <boost/fusion/adapted/boost_tuple.hpp>
#include <boost/fusion/include/boost_tuple.hpp>
#include <boost/fusion/algorithm.hpp>
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

using namespace std;

boost::tuple<int, int> find_min_max(const vector<int>& vec)
{
    auto result = minmax_element(vec.begin(), vec.end());

    return boost::make_tuple(*result.first, *result.second);
}

struct MinMax
{
    int min;
    int max;
};

template <typename Tuple, typename Func, size_t Index>
struct ForEachHelper
{
    static void for_each(const Tuple& t, Func f)
    {
        ForEachHelper<Tuple, Func, Index-1>::for_each(t, f);
        f(boost::get<Index>(t));
    }
};

template <typename Tuple, typename Func>
struct ForEachHelper<Tuple, Func, 0>
{
    static void for_each(const Tuple& t, Func f)
    {
        f(boost::get<0>(t));
    }
};

template <typename Tuple, typename Func>
void tuple_for_each(const Tuple& t, Func f)
{
    ForEachHelper<Tuple, Func, boost::tuples::length<Tuple>::value-1>::for_each(t, f);
}

template <typename Tuple, typename Func>
void simpler_foreach(const Tuple& t, Func f)
{
    f(t.get_head());
    simpler_foreach(t.get_tail(), f);
}

template <typename Func>
void simpler_foreach(const boost::tuples::null_type, Func f)
{}

class MyFormatter
{
public:
    void operator()(int x) const
    {
        cout << "<int>" << x << "</int>\n";
    }

    void operator()(double d) const
    {
        cout << "<double>" << d << "</double>\n";
    }

    void operator()(const string& s) const
    {
        cout << "<string>" << s << "</string>" << endl;
    }
};

struct Person
{
    string name;
    int id;
    int age;
};

BOOST_FUSION_ADAPT_STRUCT(
        Person,
        (string, name)
        (int, id)
        (int, age)
)

int main()
{
    typedef boost::tuple<int, double, string> MyTuple;
    MyTuple triple(8, 3.14, "Krotka");

    cout << boost::tuples::set_delimiter(',') <<  "triple: " << triple << endl;

    cout << "Element 1: " << triple.get<0>() << "\n"
         << "Element 2: " << boost::tuples::get<1>(triple) << "\n"
         << "Element 3: " << boost::tuples::get<2>(triple) << "\n";


    boost::tuple<int, float, string> other = triple;

    cout << "other: " << other << endl;

    vector<int> vec = { 6, 3, 6, 1, 665, 23, 66, 234 };

    boost::tuple<int, int> minmax = find_min_max(vec);

    cout << "minmax = " << minmax << endl;

    int x = 10;
    const double dx = 0.01;

    boost::tuple<int&, const double&> ref_tuple(x, dx);

    ref_tuple.get<0>()++;

    cout << "x: " << x << endl;

    //ref_tuple.get<1>()++;

    boost::tuple<int&, const double&> other_ref_tuple
            //= boost::make_tuple(boost::ref(x), boost::cref(dx));
            = boost::tie(x, dx);

    ++x;

    cout << "other_ref_tuple: " << other_ref_tuple << endl;


    int min, max;

    boost::tie(min, max) = find_min_max(vec);

    cout << "min = " << min << "; max = " << max << endl;

    boost::tie(min, boost::tuples::ignore) = find_min_max(vec);

    MinMax result_minmax;

    boost::tie(result_minmax.min, result_minmax.max) = find_min_max(vec);

    cout << "result_minmax.min = " << result_minmax.min << endl;

    cout << "\n\n";

    cout << "Length: " << boost::tuples::length<MyTuple>::value << endl;

    tuple_for_each(triple, MyFormatter());
    cout << "\n\nminmax: " << endl;

    tuple_for_each(boost::tie(min, max), MyFormatter());

    cout << "\n\n";
    simpler_foreach(triple, MyFormatter());

    cout << "\n\n";

    boost::fusion::for_each(triple, MyFormatter());

    cout << "\n\n";
    Person p { "Kowalski", 1, 44 };

    boost::fusion::for_each(p, MyFormatter());
}

