#include <string>
#include <iostream>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>

class PersonalInfo
{
	std::string first_name_;
	std::string last_name_;
	unsigned int age_;
public:
	PersonalInfo(const std::string& fn, const std::string& ln, unsigned int age) :
		first_name_(fn), last_name_(ln), age_(age)
	{
	}

	std::string first_name() const
	{
		return first_name_;
	}

	std::string last_name() const
	{
		return last_name_;
	}

	unsigned int age() const
	{
		return age_;
    }

    bool operator <(const PersonalInfo& pi) const
    {
        return boost::tie(last_name_, first_name_, age_)
                < boost::tie(pi.last_name_, pi.first_name_, pi.age_);
    }

    bool operator==(const PersonalInfo& pi) const
    {
        return tied() == pi.tied();
    }

public:
    boost::tuple<const std::string&, const std::string&, const unsigned int&> tied() const
    {
        return boost::tie(last_name_, first_name_, age_);
    }
};

std::ostream& operator<<(std::ostream& out, const PersonalInfo& pi)
{
	out << "PersonalInfo(" << pi.first_name() << ", " << pi.last_name()<< ", " << pi.age() << ")";

	return out;
}


//bool operator<(const PersonalInfo& pi1, const PersonalInfo& pi2)
//{
//	return pi1.last_name() < pi2.last_name();
//}
