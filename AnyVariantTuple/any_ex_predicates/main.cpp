#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <boost/any.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>

using namespace std;

template <typename T>
struct IsType
{
public:
    bool operator()(const boost::any& a) const
    {
        return a.type() == typeid(T);
    }
};

int main()
{
	vector<boost::any> store_anything;

	store_anything.push_back(1);
	store_anything.push_back(5);
	store_anything.push_back(string("three"));
	store_anything.push_back(3);
	store_anything.push_back(string("four"));
	store_anything.push_back(string("one"));
	store_anything.push_back(string("eight"));
	store_anything.push_back(5);
	store_anything.push_back(4);
	store_anything.push_back(boost::any());
	store_anything.push_back(string("five"));
	store_anything.push_back(string("six"));
	store_anything.push_back(boost::any());


	/* TO DO :
     * Wykorzystując algorytmy biblioteki standardowej wykonaj nastapujące czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
     * 1 - przefiltruj wartosci niepuste w kolekcji stored_anything
	 * 2 - zlicz ilosc elementow typu int oraz typu string
	 * 3 - wyekstraktuj z kontenera store_anything do innego kontenera wszystkie elementy typu string
     * 4 - napisz klasę MapAny
	 */

	// 1
	vector<boost::any> non_empty;

    boost::remove_copy_if(store_anything, back_inserter(non_empty),
                   std::mem_fn(&boost::any::empty));

    cout << "store_anything.size() = " << store_anything.size() << endl;
	cout << "non_empty.size() = " << non_empty.size() << endl;



	// 2
    int count_int = boost::count_if(store_anything, IsType<int>());
	// TODO
    cout << "stored_anything przechowuje " << count_int << " elementow typu int" << endl;

    int count_string = boost::count_if(store_anything, IsType<string>());
	// TODO
    cout << "stored_anything przechowuje " << count_string << " elementow typu string" << endl;

	// 3
	list<string> string_items;
    string (*to_string)(const boost::any&) = &boost::any_cast<string>;
    boost::transform(store_anything
                        | boost::adaptors::filtered(IsType<string>())
                        | boost::adaptors::reversed,
                     back_inserter(string_items), to_string);

	cout << "string_items: ";
	copy(string_items.begin(), string_items.end(),
			ostream_iterator<string>(cout, " "));
	cout << endl;
}
