#ifndef VEHICLE_HPP_
#define VEHICLE_HPP_

#include <iostream>
#include <string>

class Vehicle
{
	std::string id_;
public:
	Vehicle(const std::string& id) : id_(id)
	{
	}

	virtual ~Vehicle() {}

	std::string& id()
	{
		return id_;
	}

	const std::string& id() const
	{
		return id_;
	}

	virtual void info() const = 0;
};

class Car : public Vehicle
{
	int km_;
public:
	Car(const std::string& id) : Vehicle(id), km_(0)
	{
	}

	int km() const
	{
		return km_;
	}

	virtual void info() const
	{
		std::cout << "Car(id=" << id() << ", km=" << km() << ")" << std::endl;
	}

	void drive(unsigned int km)
	{
		km_ += km;
	}
};

class Boat : public Vehicle
{
	int sm_;
public:
	Boat(const std::string& id) : Vehicle(id), sm_(0)
	{
	}

	int sm() const
	{
		return sm_;
	}

	virtual void info() const
	{
		std::cout << "Boat(id=" << id() << ", sm=" << sm() << ")" << std::endl;
	}

	void travel(unsigned int sm)
	{
		sm_ += sm;
	}
};

#endif /* VEHICLE_HPP_ */
