#include <iostream>
#include <boost/cast.hpp>

using namespace std;

int main()
{
    int x = 7888;

    short sx = boost::numeric_cast<short>(x);

    cout << "x: " << x << endl;
    cout << "sx: " << sx << endl;

    x = -9;

    unsigned int ux = boost::numeric_cast<unsigned int>(x);

    cout << "x: " << x << endl;
    cout << "ux: " << ux << endl;
}

