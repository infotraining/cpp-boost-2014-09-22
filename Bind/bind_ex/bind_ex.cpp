#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <boost/bind.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <memory>

using namespace std;

class X
{
public:
    X() { cout << "X()" << endl; }
    ~X() { cout << "~X()" << endl; }
    int value = 10;
};

int main()
{
    {
        unique_ptr<X> uptr(new X);

        //auto ll = [uptr = move(uptr)] { cout << uptr->value << endl; };

        auto ll = std::bind([](unique_ptr<X>& ptr) { cout << ptr->value << endl; }, move(uptr));

        ll();
    }

    using namespace boost::range;
    using namespace boost::adaptors;

    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    auto print = boost::bind(&Person::print, _1);

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
    boost::for_each(employees | filtered(boost::bind(&Person::salary, _1) > 3000.0),
                    print);

    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";
    boost::for_each(employees | filtered(boost::bind(&Person::age, _1) < 30),
                    print);

    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";
    boost::sort(employees,
                boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));
    boost::for_each(employees, print);

    // wyświetl kobiety
    cout << "\nKobiety:\n";
    boost::for_each(employees | filtered(boost::bind(&Person::gender, _1) == Female),
                    print);

    // ilość osob zarabiajacych powyżej średniej
//    double avg = accumulate(employees.begin(), employees.end(), 0.0,
//                            boost::bind(plus<double>(),
//                                    _1,
//                                    boost::bind(&Person::salary, _2))) / employees.size();

    double avg = accumulate(employees.begin(), employees.end(), 0.0,
                            [](double arg1, const Person& p) { return arg1 + p.salary(); }) / employees.size();

    cout << "Avg salary: " << avg << endl;

    cout << "\nIlosc osob zarabiajacych powyzej sredniej: ";
    cout << boost::count_if(employees, boost::bind(&Person::salary, _1) > avg) << endl;
}
