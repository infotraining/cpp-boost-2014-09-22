#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/bind.hpp>
#include <vector>
#include <algorithm>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <map>

using namespace std;

void func2(int x, double y)
{
    cout << "func2(" << x << ", " << y << ")" << endl;
}

string func3(const string& text, int value, int& counter)
{
    ++counter;

    return text + " - " + boost::lexical_cast<string>(value);
}

struct Add
{
    typedef int result_type;

    int operator()(int a, int b) const
    {
        return a + b;
    }
};

class Person
{
    int id_;
    string name_;
    int age_;
public:
    Person(int id, const string& name, int age) : id_(id), name_(name), age_(age)
    {
    }

    int age() const
    {
        return age_;
    }

    bool is_retired() const
    {
        return age_ >= 67;
    }

    void print(string prefix)
    {
        cout << prefix << " " << id_ << " - " << name_ << " - " << age_ << endl;
    }
};

int main()
{
    func2(5, 6);

    int counter = 0;

    cout << func3("Test bind", 12, counter) << endl;
    cout << "counter: " << counter << endl;

    auto f1 = boost::bind(&func2, 1, 2);

    f1(); // func2(1, 2);

    auto f2 = boost::bind(&func2, _1, 7);

    f2(99); // func2(99, 7)

    auto f3 = boost::bind(&func2, 8, _1);

    f3(99); // func2(8, 99)

    auto f4 = boost::bind(&func2, _2, _1);

    f4(1, 2); // func2(2, 1)

    string prefix = "Boost::Bind";

    auto f5 = boost::bind(&func3, boost::cref(prefix), 19, boost::ref(counter));

    cout << f5() << endl;
    cout << "counter: " << counter << endl;

    Add adder;

    auto f6 = boost::bind(adder, 8, _1);

    cout << "8 + 2 = " << f6(2) << endl;

    auto f7 = boost::bind(plus<int>(), _1, 10);

    cout << "9 + 10 = " << f7(9) << endl;

    Person p(1, "Kowalski", 33);

    auto f8 = boost::bind(&Person::print, &p, "Person");

    f8();

    auto printer = boost::bind(&Person::print, _1, "Person");

    printer(p);

    vector<Person> people = { p, Person(2, "Nowak", 78), Person(5, "Anonim", 34),
                              Person(8, "Nijaki", 45), Person(666, "Jagger", 70) };

    cout << "\n\n";
    for_each(people.begin(), people.end(), boost::bind(&Person::print, _1, "Team"));

    cout << "\n\n";

    boost::for_each(people |
                        boost::adaptors::filtered(boost::bind(&Person::is_retired, _1)),
                        boost::bind(&Person::print, _1, "Retired"));

    typedef map<string, string> Dictionary;

    Dictionary dict = { { "One", "Jeden" }, { "Two", "Dwa" }, { "Three", "Trzy" },
                        { "Six", "Szesc" } };

    cout << "\n\nKeys:\n";
    boost::transform(dict, ostream_iterator<string>(cout, "\n"),
                     boost::bind(&decltype(dict)::value_type::first, _1));

//    auto gt_40 = boost::find_if(people,
//                                boost::bind(greater<int>(),
//                                            boost::bind(&Person::age, _1),
//                                            40));

     auto gt_40 = boost::find_if(people, boost::bind(&Person::age, _1) > 40);

    if (gt_40 != people.end())
        gt_40->print("Gt40");
}

