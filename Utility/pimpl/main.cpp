#include <iostream>
#include <boost/ref.hpp>
#include "bitmap.hpp"

using namespace std;

int main()
{
    Bitmap bm(10);

    bm.draw();

    auto ref_bm = boost::ref(bm);
}

