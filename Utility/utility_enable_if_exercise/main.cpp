#include <iostream>
#include <iterator>
#include <list>
#include <vector>
#include <boost/type_traits/is_pod.hpp>
#include <boost/type_traits.hpp>
#include <boost/utility/enable_if.hpp>
#include <cstring>
#include <boost/mpl/or.hpp>

using namespace std;

// 1 - napisz generyczny algorytm mcopy kopiujący zakres elementow typu T [first, last) do kontenera rozpoczynającego się od dest
// TODO
template <typename InIt, typename OutIt>
OutIt mcopy(InIt start, InIt end, OutIt out)
{
    cout << "Generic version of mcopy" << endl;
    while (start != end)
    {
        *(out++) = *(start++);
    }

    return out;
}

// 2 - napisz zoptymalizowaną wersję mcopy wykorzystującą memcpy dla tablic T[] gdzie typ T jest typem POD
// TODO

template <typename T>
typename boost::enable_if<boost::is_pod<T>, T*>::type mcopy(T* start, T* end, T* out)
{
    cout << "Optimized version of mcopy" << endl;

    size_t size = end - start;

    memcpy(out, start, size * sizeof(T));

    return out + size;
}

template <typename T, typename Enable = void>
class DataProcessor
{
public:
    void process(const vector<T>& data)
    {
        cout << "Generic DataProcessor::process" << endl;
    }
};

template <typename T>
class DataProcessor
<
    T,
    typename boost::enable_if<
        boost::mpl::or_<
            boost::is_float<T>,
            boost::is_integral<T>
        >
    >::type
>
{
public:
    void process(const vector<T>& data)
    {
        cout << "Float DataProcessor::process" << endl;
    }
};

int main()
{
    string words[] = { "one", "two", "three", "four" };

    list<string> list_of_words(4);

    mcopy(words, words + 4, list_of_words.begin()); // działa wersja generyczna

    mcopy(list_of_words.begin(), list_of_words.end(), ostream_iterator<string>(cout, " "));
    cout << "\n";

    int numbers[] = { 1, 2, 3, 4, 5 };
    int target[5];

    mcopy(numbers, numbers + 5, target); // działa wersja zoptymalizowana

    mcopy(target, target + 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    vector<int> data1;

    DataProcessor<int> dp1;

    dp1.process(data1);

    vector<double> data2;

    DataProcessor<double> dp2;

    dp2.process(data2);

    vector<string> data3;

    DataProcessor<string> dp3;

    dp3.process(data3);
}

