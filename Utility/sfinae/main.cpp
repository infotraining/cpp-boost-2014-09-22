#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>

using namespace std;

struct MyInt
{
    typedef void type;

    long value;

    MyInt(long val) : value(val) {}
};

ostream& operator<<(ostream& out, const MyInt& i)
{
    out << i.value;

    return out;
}

template <typename T>
typename boost::enable_if<boost::is_integral<T>, T>::type foo(T arg)
{
    cout << "foo(integral: " << arg << ")" << endl;
    return arg;
}

template <typename T>
typename boost::disable_if<boost::is_integral<T>, T>::type foo(T arg1)
{
    cout << "foo<T>(arg: " << arg1 << ")" << endl;

    return arg1;
}

int main()
{
    int x = 89;

    foo(x);

    short sx = 77;

    foo(sx);

    double dx = 8.99;

    cout << "Returned value: " << foo(dx) << endl;

    MyInt mx = 2;

    foo(mx);
}

