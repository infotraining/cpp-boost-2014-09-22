#include <iostream>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <memory>

class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
	BOOST_STATIC_ASSERT(boost::is_integral<T>::value);
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
	BOOST_STATIC_ASSERT(sizeof(int)==4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
	BOOST_STATIC_ASSERT((boost::is_base_of<Base, T>::value));
}

template <typename T>
struct is_auto_ptr : public boost::false_type
{};

template <typename T>
struct is_auto_ptr<std::auto_ptr<T>> : public boost::true_type
{};

template <typename T>
void works_with_pointers(T ptr)
{
    BOOST_STATIC_ASSERT(!is_auto_ptr<T>::value && "std::auto_ptr<T> nie jest akceptowanym wskaźnikiem");
    //static_assert(!is_auto_ptr<T>::value, "std::auto_ptr<T> nie jest akceptowanym wskaźnikiem");

    std::cout << "value: " << *ptr << std::endl;
}

int main()
{
    OnlyCompatibleWithIntegralTypes<int> test1;

    int arr[50];

	accepts_arrays_with_size_between_1_and_100(arr);

	Derived arg;
    std::string str;
    works_with_base_and_derived(arg);

    int x = 10;

    works_with_pointers(&x);

    std::shared_ptr<int> sp(new int(40));

    works_with_pointers(sp);

    std::auto_ptr<int> ap(new int(99));

    works_with_pointers(ap);

    std::cout << *ap << std::endl;
}
