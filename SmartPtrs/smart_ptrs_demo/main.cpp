#include <iostream>
#include <string>
#include <memory>
#include <cassert>
#include <vector>
#include <functional>
#include <boost/scoped_ptr.hpp>

using namespace std;

class Object
{
    int id_;
public:
    Object(int id = 0) : id_(id)
    {
        cout << "Object(" << id_ << ")" << endl;
    }

    ~Object()
    {
        cout << "~Object(" << id_ << ")" << endl;
    }

    void run()
    {
        cout << "run" << endl;
    }

    int id() const
    {
        return id_;
    }
};

void cleanup_object(Object* ptr)
{
    delete ptr;
}

std::auto_ptr<Object> create_object()
{
    static int id = 100;

    return std::auto_ptr<Object>(new Object(++id));
}

void use_and_sink(auto_ptr<Object> ptr)
{
    cout << "Using obejct with id " << ptr->id() << endl;
}

namespace unique
{
    unique_ptr<Object> create_object()
    {
        static int id = 100;

        return unique_ptr<Object>(new Object(++id));
    }

    void use_and_sink(unique_ptr<Object> ptr)
    {
        cout << "Using obejct with id " << ptr->id() << endl;
    }
}

int main()
{
    {
        auto_ptr<Object> ptr1(new Object(1));

        cout << "id: " << ptr1->id() << endl;

        auto_ptr<Object> ptr2 = ptr1;

        assert(ptr1.get() == 0);

        cout << "id: " << ptr2->id() << endl;

        ptr2.reset(new Object(2));

        cleanup_object(ptr2.release());

        auto_ptr<Object> ptr3 = create_object();

        use_and_sink(create_object());

        boost::scoped_ptr<Object> sc_ptr(create_object());

        cout << "sc_ptr->id() = " << sc_ptr->id() << endl;

        cout << "end of scope" << endl;
    }

    cout << "\n\n";

    {
        unique_ptr<Object> ptr1(new Object(1));

        cout << "id: " << ptr1->id() << endl;

        unique_ptr<Object> ptr2 = move(ptr1);

        assert(ptr1.get() == nullptr);

        cout << "id: " << ptr2->id() << endl;

        unique_ptr<Object> ptr3 = unique::create_object();

        cout << "id: " << ptr3->id() << endl;

        unique::use_and_sink(move(ptr3));

        unique::use_and_sink(unique::create_object());

        vector<unique_ptr<Object>> vec;

        vec.push_back(move(ptr2));
        vec.push_back(unique_ptr<Object>(new Object(13)));
        vec.push_back(create_object()); // create_object zwraca auto_ptr
        vec.push_back(unique::create_object());

        cout << "\nforeach\n";

        for(const auto& ptr : vec)
        {
            cout << "id: " << ptr->id() << endl;
        }

        cout << "\nforeach\n";


        unique_ptr<Object> ptr4 = move(vec[0]);
    }

    cout << "\n\n";

    {
        unique_ptr<Object[]> arr1(new Object[10]);

        cout << "id: " << arr1[0].id() << endl;
    }

    {
        std::function<void ()> f;

        {
            std::shared_ptr<Object> sp(new Object(665));


            f = std::bind(&Object::run, sp);

            cout << "rc: " << sp.use_count() << endl;
            cout << "end of scope" << endl;
        }

        f();
    }
}


