#include <iostream>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using namespace std;

void foo(int x, double& dx)
{
    cout << "x: " << x << ", dx: " << dx << endl;
}

struct Foo
{
    void operator()(int x, double& dx)
    {
        cout << "Foo -> x: " << x << ", dx: " << dx << endl;
    }
};

class Service
{
public:
    typedef boost::function<void (string)> OnRunCallback;
    typedef boost::function<void()> OnCloseCallback;

    void run(const string& msg)
    {
        if (on_run_callback_)
            on_run_callback_(msg);

        cout << "Service is running with: " << msg << endl;
    }

    void close()
    {
        if (on_close_callback_)
            on_close_callback_();

        cout << "Service is closed..." << endl;
    }

    void set_on_run_callback(OnRunCallback callback)
    {
        on_run_callback_ = callback;
    }

    void set_on_close_callback(OnCloseCallback callback)
    {
        on_close_callback_ = callback;
    }

private:
    OnRunCallback on_run_callback_;
    OnCloseCallback on_close_callback_;
};

namespace MyLogger
{
void log(const string& msg)
{
    cout << "Log: " << msg << endl;
}
}

class Database
{
    string name = "Database";

public:
    ~Database()
    {
        cout << "~Database()" << endl;
    }

    void save_to_db(const string& table_name, const string& column_name, const string& data) const
    {
        cout << "Saving to " << name << endl;
        cout << "INSERT INTO " << table_name << "(" << column_name << ") VALUES(" << data << ")" << endl;
    }
};

int main()
{
    Service srv;

    {
        boost::shared_ptr<Database> db = boost::make_shared<Database>();

        srv.set_on_run_callback(&(MyLogger::log));

        srv.run("Start");
        srv.run("Data");

        srv.set_on_close_callback(boost::bind(
                                      &Database::save_to_db, db,
                                      "Logs", "CloseEvents", "Service is closed"));

            srv.set_on_run_callback(boost::bind(
                                          &Database::save_to_db, db,
                                          "Logs", "RunEvents", _1));

//        srv.set_on_run_callback(
//                    [=](const string& msg) { db->save_to_db("Logs", "RunEvents", msg);});
    }


    srv.run("Data3");

    srv.close();
}

